# 3_2 格式化产品信息 Format product information

## 题目

编写一个程序，对用户录入的产品信息进行格式化。程序会话应类似下面这样。

## 样例

### 样例一

        Enter item number: 583
        Enter unit price: 13.5
        Enter purchase date (mm/dd/yyyy) : 10/24/2010
        Item        Unit        Purchase
                    Price       Date
        583         $ 13.50     10/24/2010

### 样例二

        Enter item number: 669
        Enter unit price: 12.5
        Enter purchase date (mm/dd/yyyy) : 1/1/2011
        Item        Unit        Purchase
                    Price       Date
        669         $ 12.50     01/01/2011

## 数据范围

输入的 item 为整数。
输入的 price 为浮点数（保留一位小数）。
输入的 date 为规范日期，若为个位数进行补齐。
输出的 price 为浮点数（保留两位小数）。
按照题目内容中的输出为规范输出。

## 提示

其中，产品编号和日期项采用左对齐方式，单位价格采用右对齐方式，允许最大取值为 9999.99 的美元。
各个列使用制表符控制。
为保持对齐，Item 和 Unit 间有两个制表符，Unit 与 Pruchase之间有三个制表符。Price 与 Date 首字母与 U 和 P 对齐。