# 7_6 数值显示 Show Sizeof

## 题目

编写程序显示 sizeof(int) 、sizeof(short) 、sizeof(float) 、 sizeof(double) 和 sizeof(long double)的值。

## 样例

### 样例一

    Size of int: 4
    Size of short: 2
    Size of long: 8
    Size of float: 4
    Size of double: 8
    Size of long double: 16

## 提示

需要使用 `sizeof` 函数显示。