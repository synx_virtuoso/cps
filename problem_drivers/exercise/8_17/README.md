# 8_17 幻方 Magic square

## 题目

编写程序打印 n x n 的幻方（1，2，...，n<sup>2</sup> )的方阵排列，且每行、每列和对角线上的和都相等）。由用户指定 n 的值。把幻方存储在一个二维数组中。起始时把数 1 放在 0 行的中间，剩下的数 2，3，...，n<sup>2</sup> 依次向上移动一行并向右移动一列。当可能越过数组边界时需要 “绕回” 到数组的另一端。例如，如果需要把下一个数放到 -1 行，我们就将其存储到 n-1 行（最后一行）; 如果需要把下一个数放到 n 列，我们就将其存储到 0 列。如果某个特定的数组元素已被占用，那就把该数存储在前一个数的正下方。如果你的编译器支持变长数组，声明数组有 n 行 n 列，否则声明数组有 99 行 99 列。

## 样例

### 样例一

    This program creates a magic square of a specified size.
    The size must be an odd number between 1 and 99.
    Enter size of magic square: 5

        17   24    1    8   15
        23    5    7   14   16
         4    6   13   20   22
        10   12   19   21    3
        11   18   25    2    9

### 样例二

    This program creates a magic square of a specified size.
    The size must be an odd number between 1 and 99.
    Enter size of magic square: 7
        30    39    48     1    10    19    28
        38    47     7     9    18    27    29
        46     6     8    17    26    35    37
        5     14    16    25    34    36    45
        13    15    24    33    42    44     4
        21    23    32    41    43     3    12
        22    31    40    49     2    11    20

## 数据范围

输入数据为 1 - 99 的奇数。
