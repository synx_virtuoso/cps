# 打印 UPC Print UPC

## 题目编号 5-6

## 题目

修改教材 4.1 节的 upc.c 程序，使其可以检测 UPC 的有效性。
在用户输入 UPC 后，程序将输出 `VALID` 或 `NOT VALID` 。

## 样例

### 样例一

    Enter the first (single) digit: 0
    Enter first group of five digits: 13800
    Enter second group of five digits: 15173
    Enter the last (single) digit: 5
    VALID

### 样例二

    Enter the first (single) digit: 0
    Enter first group of five digits: 51500
    Enter second group of five digits: 24128
    Enter the last (single) digit: 7
    NOT VALID

## 数据范围

按照题目要求，first digit 为 一个数字（大部分商品用 0 或 7 表示，
2表示需要称量的商品，3表示药品或与健康相关的商品，而 5 表示赠品），
last digit 可能是 0～9 中的任意一个数字，first group of five digits
和 second group of five digits 均由 5 个数字组成。
