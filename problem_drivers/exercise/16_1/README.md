# 16_1 根据电话区号显示国家名称 Display country name

## 题目

编写程序用来要求用户录入国际电话区号，然后在数组 country_codes 中查找它（见 16.3 节）。如果
找到对应的区号，程序需要显示相应的国家名称，否则显示出错消息。

## 样例

### 样例一

    Enter dialing code: 86
    The country with dialing code 86 is China

### 样例二

    Enter dialing code: 0
    No corresponding country found

## 数据范围

输入的区号可以是书中已定义的也可以是书中未定义的。
