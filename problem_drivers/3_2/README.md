﻿# 3_2 表达式求值 Expression evaluation

## 题目

编写程序对表达式求值。

## 样例

### 样例一

    Enter an expression: 1+2.5*3
    Value of expression: 10.5

### 样例二

    Enter an expression: 2+3*4-5/2
    Value of expression: 7.5

## 数据范围

表达式中的操作数是浮点数，表达式从左向右求值（所有运算符的优先级都一样）。可以假设输入的表达式保证合法。运算结果保留一位小数。