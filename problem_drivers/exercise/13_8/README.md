# 13_8 拼字游戏 Anyword game

## 题目

修改第 7 章的编程题 5，使其包含如下函数：

    int compute_scrabble_value(const char *word);

函数返回 word 所指向的字符串的拼字值。

## 样例

### 样例一

    Enter a word: pitfall
    Scrabble value: 12

### 样例二

    Enter a word: Hello
    Scrabble value: 8

## 数据范围

输入的单词可以为混合的大小写字母。但不允许出现除字母以外的字符。

## 提示

使用 toupper 库函数。