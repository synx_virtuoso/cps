# 13_9 统计句子中元音字母个数 Sum the number of vowel

## 题目

修改第 7 章的编程题 10，使其包含如下函数：

    int compute_vowel_count(const char *word);

函数返回 sentence 所指向的字符串中元音字母的个数。

## 样例

### 样例一

    Enter a sentence: And that's the way it is.
    Your sentence contains 6 vowels

### 样例二

    Enter a sentence: BMW.
    Your sentence contains 0 vowels.

## 数据范围

输入的句子中可以包括字母（大小写）及其它字符。
