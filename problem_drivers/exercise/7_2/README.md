# 7-2 修改 square.c 程序 modify square.c：

## 题目

修改 6.3 节的程序 square.c，每 24 次平方后暂停并显示下列信息：

    Press Enter to continue...
显示完上述消息后，程序应该使用 getchar 函数读入一个字符。getchar 函数读到用户录入的回车键才允许程序继续。

## 样例

### 样例一

    This program prints a table of squares.
    Enter number of entries in table: 38
            1         1
            2         4
            3         9
            4        16
            5        25
            6        36
            7        49
            8        64
            9        81
            10       100
            11       121
            12       144
            13       169
            14       196
            15       225
            16       256
            17       289
            18       324
            19       361
            20       400
            21       441
            22       484
            23       529
            24       576
    Press Enter to continue...
            25       625
            26       676
            27       729
            28       784
            29       841
            30       900
            31       961
            32      1024
            33      1089
            34      1156
            35      1225
            36      1296
            37      1369
            38      1444

### 样例二

    This program prints a table of squares.
    Enter number of entries in table: 25
            1         1
            2         4
            3         9
            4        16
            5        25
            6        36
            7        49
            8        64
            9        81
            10       100
            11       121
            12       144
            13       169
            14       196
            15       225
            16       256
            17       289
            18       324
            19       361
            20       400
            21       441
            22       484
            23       529
            24       576
    Press Enter to continue...
            25       625

## 数据范围

假设输入的数范围在 0 - 100 之间。要求程序在读到非‘\n‘字符时结束程序。
