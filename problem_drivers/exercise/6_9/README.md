# 6_9 计算剩余还款金额 (改 2.8)

## 题目

第二章的编程题 8 要求编程计算第一，第二，第三个月还贷后剩余的贷款金额，修改该程序，要求用户输入还贷的次数并显示每次还贷后剩余的贷款金额。

## 样例

### 样例一

      Enter amount of loan: 20000.00
      Enter interest rate: 6.0
      Enter monthly payment: 385.66
      Enter number of payments: 3
      Balance remaining after 1 month(s): $19714.34
      Balance remaining after 2 month(s): $19427.25
      Balance remaining after 3 month(s): $19138.73

### 样例二

      Enter amount of loan: 20000.00
      Enter interest rate: 6.0
      Enter monthly payment: 386.66
      Enter number of payments: 4
      Balance remaining after 1 month(s): $19713.34
      Balance remaining after 2 month(s): $19425.25
      Balance remaining after 3 month(s): $19135.71
      Balance remaining after 4 month(s): $18844.73

## 数据范围

设输入的天数为`n`，则 n ≥ 0

## 提示

1. 按照题目内容中的输出为规范格式进行输出。