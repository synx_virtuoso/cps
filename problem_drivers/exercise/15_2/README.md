# 15_2 文本格式化 Justify

## 题目

修改 15.3 节 的 justify 程序，在 read_word 函数（而不是 main 函数）为被截短的单词的结尾存储 *字符。当单词大于 20 个字符时会被截断，只需上传包含 void read_word(char *word, int len) 函数的.c 文件。

## 样例

### 样例一

文件内容如下：

    antidisestablishmentarianism

输出

    antidisestablishment*

### 样例二

文件内容如下：

    hello

输出:

    hello

## 数据范围

在文件中可以输入大于 20 个字符的单词，也可以输入小于 20 个字符的单词。单词可以包含任意字符。