# 22_13 根据文件数据计算飞机起飞时间 Search flight time from file

## 题目

修改第 5 章的编程题 8，让程序从名为 flights.dat 的文件中获取起飞时间和抵达时间。
文件中的每一行先给出起飞时间再给出抵达时间，中间用一个或多个空格隔开，时间用
24 小时制表示，文件包含的是原题中航班的信息，则 flights.dat 如下：

    8:00   10:16
    9:43   11:52
    11:19  13:31
    12:47  15:00
    14:00  16:08
    15:45  17:55
    19:00  21:20
    21:45  23:58

## 样例

### 样例一

    Enter a 24-hour time: 14:05
    Closest departure time: 14:00  arriving at: 16:08

### 样例二

    Enter a 24-hour time: 6:00
    Closest departure time: 8:00  arriving at: 10:16

## 数据范围

flights.dat 中的航班时间可以任意设定，在标准输入中输入的时间范围时 0:00 到 24:00 。