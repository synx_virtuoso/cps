# 5_9 判断更早的日期

## 题目

编程题5.9 编写一个程序，提示用户输入两个日期，然后显示哪一个日期更早：

    Enter first date (mm/dd/yy): 3/6/08
    Enter second date (mm/dd/yy): 5/17/07
    5/17/07 is earlier than 3/6/08

## 样例

### 样例一

    Enter first date (mm/dd/yy): 3/6/08
    Enter second date (mm/dd/yy): 5/17/07
    5/17/07 is earlier than 3/6/08

### 样例二

    Enter first date (mm/dd/yy): 5/18/07
    Enter second date (mm/dd/yy): 3/6/08
    5/18/07 is earilier than 3/6/08

## 数据范围

1. 设输入的年份为`y`，则 00 ≤ y ≤ 99；

2. 设输入的月份为`m`，则 1 ≤ m ≤ 12;

3. 每月天数应满足每月应有的天数;

4. 以上数据应全部为整数;

## 提示

按照题目内容中的输出为规范格式进行输出。
