# 将百分制成绩转换为等级 Transform the Grade to Level

## 题目编号 5-10

## 题目

利用 switch 语句编写一个程序，把用数字表示的成绩转化为字母表示的等级。
使用下面的等级评定规则：A 为 90~100，B 为 80~89，C 为 70~79，D 为 60~69，
F 为 0~59。如果成绩高于 100 或低于 0 显示出错信息。

## 样例

### 样例一

    Enter numerical grade：84
    Letter grade：B

### 样例二

    Enter numerical grade：-1
    Error, grade must be between 0 and 100.

## 数据范围

输入成绩为整数，输出为符合题目规定的等级或错误信息。

## 提示

把成绩拆分成 2 个数字，然后使用 switch 语句判定十位上的数字。


## 测试用例

95
90
100
85
80
89
75
70
79
65
60
69
30
0
59
101
102
-2
-1
200
