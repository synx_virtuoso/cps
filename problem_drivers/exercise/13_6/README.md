# 13_6 核对行星名字 Check planet

## 题目

改进 13.7 节的程序 planet.c，使它在对命令行参数和 planets 数组中的字符串进行比较时忽略大小写。

## 样例

### 样例一

输入：

    planet Jupiter venus Earth fred

输出：

    Jupiter is planet 5
    venus is not a planet
    Earth is planet 3
    fred is not a planet

### 样例二

输入：

    planet URANUS NEPTUNE PLUTO hello

输出：

    URANUS is planet 7
    NEPTUNE is planet 8
    PLUTO is planet 9
    hello is not a planet

## 数据范围

命令行中的参数为行星的名称（不区分大小写），或者为一个任意的字符串。