# 1_1 税率计算 Taxcal

## 题目

编写一个程序，要求用户输入一个美元数量，然后显示出增加5%税率后的相应金额。

## 样例

### 样例一

    Enter an amount: 100.00
    With tax added: $105.00

### 样例二

    Enter an amount: 50
    With tax added: $52.50

## 数据范围

输入数据为 0 - 10000 的数据。要求输出数据保留两位小数。