#include <stdio.h>

int main(void)
{
    int days_in_month, week_start;

    printf("Enter number of days in month: ");
    scanf("%d", &days_in_month);
    printf("Enter starting day of the week (1=Sun, 7=Sat): ");
    scanf("%d", &week_start);

    int i;
    // 所需要打印的次数，包括占位的空格和日期
    int iterations_needed = days_in_month + (week_start - 1);
    int day = 1; 

    for (i = 1; i <= iterations_needed; i++) {
        // 小于开始天数的时候，空格占位
        if (i < week_start)
            printf("   ");
        // 输出日期
        else
            printf("%3d", day++);
        // 每打印7次，第七次，需要换行
        if (i % 7 == 0)
            printf("\n");
    }

    return 0;
}
