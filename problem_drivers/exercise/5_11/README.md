# 5_11 两位数字转单词

## 题目

编写一个程序，要求用户输入一个两位数，然后显示该数的英文单词:

    Enter a two-digit number: 45
    You entered the number forty-five.

## 样例

### 样例一

    Enter a two-digit number: 45
    You entered the number forty-five.

### 样例二

    Enter a two-digit number: 85
    You entered the number eighty-five.

## 数据范围

设输入的数为 `n` ，则 10 ≤ n ≤ 99

## 提示

1. 把数分解为两个数字。用一个switch语句显示第一位数字对应的单词("twenty"、"thirty"等)用第二个switch语句显示第二位数字对应的单词。不要忘记10-19需要特殊处理。

2. 按照题目内容中的输出为规范格式进行输出。
