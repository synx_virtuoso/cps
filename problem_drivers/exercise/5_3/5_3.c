#include <stdio.h>

int main(){
    float competitor, commission, perValue;
    int num;

    printf("Enter the number of shares: ");
    scanf("%d", &num);
/*
    if(num < 0) {
        return 1;
    }
*/
    printf("Enter the value of per share: ");
    scanf("%f", &perValue);
/*
    if(perValue < 0){
        return 1;
    }
*/
    float value = perValue * num;
    if(value < 2500.00f){
        commission = 30.00f + 0.017f * value;
    }
    else if(value < 6250.00f){
        commission = 56.00f + 0.0066f * value;
    }
    else if(value < 20000.00f){
        commission = 76.00f + 0.0034f * value;
    }
    else if(value < 50000.00f){
        commission = 100.00f + 0.0022f * value;
    }
    else if(value < 500000.00f){
        commission = 155.00f + 0.0011f * value;
    }
    else{
        commission = 255.00f + 0.0009f * value;
    }

    if(commission < 39.00f){
        commission = 39.00f;
    }

    if(num < 2000){
        competitor = 33.00 + 0.03 * num;
    }
    else{
        competitor = 33 + 0.02 * num;
    }

    printf("Commission: $%.2f\n", commission);
    printf("Competitor commission: $%.2f\n", competitor);
    return 0;
}
